-- return the customerName of the customers who are from the Philippines
SELECT customerName FROM customers WHERE country = "Philippines";

-- return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- return the product name and MSRP of the product named "The Titanic"
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- return the names of customers who have no registered state
SELECT customerName FROM customers WHERE state IS NULL;

-- return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- return the customer names of customers whose customer names dont have 'a' in them
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

-- return the customer numbers of orders whose comments contain the string 'DHL'
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- return the countries of customers without duplication
SELECT DISTINCT country FROM customers;

-- return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;

-- return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

-- return the first name, last name, and office's city of employees whose offices are in tokyo
SELECT employees.firstName, employees.lastName, offices.city FROM employees
	JOIN offices ON offices.officeCode = employees.officeCode WHERE offices.city = "Tokyo";

-- return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customers.customerName FROM customers
	JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE 		employees.firstName = "Leslie" AND employees.lastName = "Thompson";

-- return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT productName, customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
    JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
    JOIN products ON orderdetails.productCode = products.productCode
    WHERE customers.customerName = "Baane Mini Imports";

-- return the employees' first names, employees' last names, customer's names, and offices' coutnries of transactions whose customers and offices are in the same country
SELECT firstName, lastName, customerName, offices.country FROM employees
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
    JOIN offices ON employees.officeCode = offices.officeCode
    WHERE offices.country = customers.country;

-- return the last names and first names of employees being supervised by "Anthony Bow"
SELECT lastName, firstName FROM employees WHERE reportsTo = (SELECT employeeNumber FROM employees WHERE firstName = "Anthony" AND lastName = "BOW");

-- return the product name and MSRP of the product with the highest MSRP
SELECT productName, MSRP FROM products ORDER BY MSRP DESC LIMIT 0,1;

-- return the number of customers in the UK
SELECT COUNT(*) FROM customers WHERE country = "UK";

-- return the number of products per product line
SELECT productlines.productLine, COUNT(*) FROM productlines
	JOIN products ON products.productLine = productlines.productLine
    GROUP BY productlines.productLine;

-- return the number of customers served by every employee
SELECT firstName, lastName, COUNT(*) FROM employees
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
    GROUP BY employeeNumber;

-- return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
SELECT productName, quantityInStock FROM products
	JOIN productlines ON products.productLine = productlines.productLine
    WHERE productlines.productLine = "planes" AND products.quantityInStock < 1000;